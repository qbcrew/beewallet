const config = require('config');
const RippleAPI = require('ripple-lib').RippleAPI;
const fwWallet = require('../schemas/fiat_wallet');
const wModel = require('../schemas/wallet');
const api = new RippleAPI({
    server: config.get('xrp.uri') // Public rippled server hosted by Ripple, Inc.
});
api.on('ledger', function (ledger) {
    api.getLedger({
        ledgerVersion: ledger.ledgerVersion,
        includeAllData: true,
        includeTransactions: true
    }).then(ledger => {
        return ledger.transactions && ledger.transactions.forEach(async (item) => {
            if (!item.outcome.deliveredAmount || item.outcome.deliveredAmount.currency !== 'XRP') return;
            let from = item.specification.source.address;
            let to = item.specification.destination.address;
            let wallets = await wModel.find({
                '$or': [{ main_address: to }, { main_address: from }],
                coin_type: 'xrp'
            });
            wallets.length && wallets.forEach(async (wallet) => {
                let transaction = {};
                transaction.crypto_amount = item.outcome.deliveredAmount.value;
                transaction.user = wallet.user;
                transaction.type = wallet.main_address === item.from ? 'SENT' : 'RECEIVED';
                transaction.coin = 'xrp';
                transaction.confirmations = 1;
                transaction.status = 'COMPLETED';
                transaction.blockhash = ledger.ledgerHash;
                transaction.blockindex = ledger.ledgerVersion;
                transaction.txid = ledger.transactionHash;
                transaction.hex = ledger.transactionHash;
                return fwWallet.update({ txid: item.hash }, transaction, { upsert: true })
                    .then(() => {
                        return getBalance(wallet.main_address).then(amount => {
                            wModel.update(
                                { main_address: wallet.main_address },
                                { confirmed_amount: amount}
                            ).then(console.log)
                        });
                    });
            });
        })

    })
})
api.on('error', (errorCode, errorMessage) => {
    console.log(errorCode + ': ' + errorMessage);
});
api.on('connected', () => {
    console.log('RIPPLE connected');

    //sample call
    // transferAmount('rNg5FfYdvi7gRbBzQ2oznUCL3DCbcuae9J', 'raHGZziDBmJmJFTYehvtXWFQxiwu1nvqAB', 20, { password: 'shALaLUr3c4u3SvtvQjX8z3n9uN41' }).then(console.log)
});
api.on('disconnected', (code) => {
    // code - [close code](https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent) sent by the server
    // will be 1000 if this was normal closure
    console.log('RIPPLE disconnected, code:', code);
    connectRipple(console.log)
});
const connectRipple = (cb) => {
    api.connect().then(cb).catch(cb);
};

/**
 * Create wallet function
 * @returns {String} Return address
 */

const createWallet = async () => {
    return api.generateAddress();
};
/**
 * Create mulit signature wallet function
 * @param coin {String} currnecy name
 * @param account {String} account name
 * @returns {String} Return multisig wallet address
 */
const createMultiSigWallet = async (coin, account) => {
};
/**
 * Create mulit signature wallet function
 * @param {String} address address
 * @returns {Number} Return balance of particular address
 */
const getBalance = async (address) => {
    let result = await api.getBalances(address, { currency: 'XRP' });
    if (result.length) return result[0].value;
};
/**
 * Create mulit signature wallet function
 * @param {String} address from address
 * @param {String} password from address secret
 * @param {Object} option will contain to address and amount
 * @returns {String} Return transaction hex
 */

const transferAmount = async (from, to, amount, options) => {
    amount = String(amount);
    let payment = {
        "source": {
            "address": from,
            "maxAmount": {
                "value": amount,
                "currency": "XRP"
            }
        },
        "destination": {
            "address": to,
            "amount": {
                "value": amount,
                "currency": "XRP"
            }
        }
    };
    let result = await api.preparePayment(from, payment);
    const prepared = result.txJSON;
    const secret = options.password;
    sign = api.sign(prepared, secret);
    let resultS = await api.submit(sign.signedTransaction)
    console.log(resultS)
    return sign.id;
}
/**
 * @param {String} tx transaction id
 * @return transaction object
 */
const getTransaction = async (tx) => {
    return await api.getTransaction(tx, { limit: 1 })
}

module.exports = {
    createWallet
    , connectRipple
    , getBalance
    , transferAmount
    , createMultiSigWallet
    , getTransaction
};