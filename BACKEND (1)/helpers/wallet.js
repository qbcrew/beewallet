let wLib = require("../lib/wallet");
const tModel = require('../schemas/transaction');
const fwModel = require('../schemas/fiat_wallet');
const wModel = require('../schemas/wallet');
const transferAmount = async (coin_type, from, to, amount, options) => {
    console.log('TRANSFER AMOUNT OBJ = ', { coin_type, from, to, amount, options })
    switch (coin_type) {
        case 'btc':
            tx = await wLib.btc.transferAmount(from, to, amount);
            break;
        case 'ltc':
            amount = String(amount);
            tx = await wLib.ltc.transferAmount(from, to, amount);
            break;
        case 'bch':
            tx = await wLib.bch.transferAmount(from, to, amount);
            break;
        case 'eth':
            console.log('ETH OBJ = ', wLib.eth)
            tx = await wLib.eth.transferAmount(from, to, amount, { password: options.password });
            break;
        case 'debc':
            tx = await wLib.debc.transferAmount(from, to, amount, { password: options.password });
            break;
        case 'xrp':
            tx = await wLib.xrp.transferAmount(from, to, amount, { password: options.password });
            break;
        default:
            if (!coin_type) throw new Error('Invalid coin type.');
            break;
    }
    console.log('TX = ', tx);
    return tx;
};
const getTransaction = async (coin, txid) => {
    try {
        let fwArray = [];
        if (['btc', 'bch', 'ltc'].indexOf(coin) !== -1) {
            let tData = await wLib[coin].getTransaction(txid);
            tData.coin = coin;
            if (tData.details && tData.details.length) {
                tData.details.forEach(async (item) => {
                    let fw = {};
                    fw.type = item.category.toUpperCase();
                    fw.amount = 0;
                    fw.gateway = 'CRYPTO';
                    fw.coin = coin;
                    fw.crypto_amount = parseFloat(item.amount);
                    fw.address = item.address;
                    fw.user = item.account;
                    fw.txid = txid;
                    fw.confirmations = tData.confirmations;
                    fw.status = 'COMPLETED';
                    fwArray.push(fw);
                });
            }
            return fwArray;
        }
        else return []
    }
    catch (ex) {
        console.log('TX WEB HOOKS ERROR = ', ex);
        return [];
    }
}
ETHSubscribe = () => {
    wLib.eth.subscribe(async (error, data) => {
        try {
            // console.log('DATA = ',error, data)
            let transactions = data.transactions;
            if (!transactions) return;
            transactions.forEach(async (item) => {
                let wallets = await wModel.find({ '$or': [{ main_address: item.from && item.from.toLowerCase() }, { main_address: item.to && item.to.toLowerCase() }], coin_type: 'eth' });
                wallets.forEach(async (wallet) => {
                    let transaction = {};
                    transaction.crypto_amount = item.value;
                    transaction.user = wallet.user;
                    transaction.type = wallet.main_address === item.from ? 'SENT' : 'RECEIVED';
                    transaction.coin = 'eth';
                    transaction.confirmations = 1;
                    transaction.blockhash = item.blockHash;
                    transaction.blockindex = item.blockNumber;
                    transaction.txid = item.hash;
                    transaction.hex = item.hash;
                    transaction.status = 'COMPLETED';
                    return fwWallet.update({ txid: item.hash }, transaction, { upsert: true })
                        .then(() => {
                            wLib.eth.getBalance(wallet.main_address).then(amount => {
                                wModel.update(
                                    { main_address: wallet.main_address },
                                    { confirmed_amount: amount }
                                ).then(console.log)
                            });
                        });
                });
            })
        }
        catch (ex) {
            console.log('ETH ERROR = ', ex);
            return;
        }
    })
}
ETHSubscribe();
module.exports = { transferAmount, ETHSubscribe, getTransaction };